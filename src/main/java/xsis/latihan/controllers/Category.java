package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;


@Controller
@RequestMapping(value="/category/")
public class Category	{
	
	@Autowired
	private CategoryRepo categoryrepo;
	
	@GetMapping(value="index")
	public ModelAndView index () {
		ModelAndView view = new ModelAndView("/category/index");
		List<CategoryModel> category = this.categoryrepo.findAll();
		view.addObject("category",category);
		return view;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView view = new ModelAndView("/category/form");
		CategoryModel categorymodel = new CategoryModel();
		view.addObject("category", categorymodel);
		return view;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute CategoryModel categorymodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/category/form");
		}else {
			this.categoryrepo.save(categorymodel);
			return new ModelAndView("redirect:/category/index");
		}
	}
	
	@GetMapping(value="edit/{CategoryId}")
	public ModelAndView edit(@PathVariable("CategoryId") Long CategoryId) {
	ModelAndView view = new ModelAndView("/category/form");
	CategoryModel categorymodel = this.categoryrepo.findById(CategoryId).orElse(null);
	view.addObject("category", categorymodel);
	return view;	
		
	}
	
	@GetMapping(value="delete/{CategoryId}")
	public ModelAndView delete(@PathVariable("CategoryId") Long CategoryId) {
	ModelAndView view = new ModelAndView("/category/delete");
	CategoryModel categorymodel = this.categoryrepo.findById(CategoryId).orElse(null);
	view.addObject("category", categorymodel);
	return view;	
		
	}
	
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute CategoryModel categorymodel, BindingResult bindingresult) {
		if(bindingresult.hasErrors()) {
			return new ModelAndView("redirect:/category/form");
		}else {
			this.categoryrepo.delete(categorymodel);
			return new ModelAndView("redirect:/category/index");
		}
	}
	
	
	
}
