package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="variant")
public class VariantModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long Id;
	
	@Column(name="name")
	private String Name;
	
	@Column(name="category_id")
	private Integer CategoryId;
	
	@ManyToOne
	@JoinColumn(name="category_id", insertable = false, updatable=false)
	private CategoryModel categorymodel;

	public long getId() {
		return Id;
	}

	public void setId(long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public Integer getCategoryId() {
		return CategoryId;
	}

	public void setCategoryId(Integer categoryId) {
		CategoryId = categoryId;
	}

	public CategoryModel getCategorymodel() {
		return categorymodel;
	}

	public void setCategorymodel(CategoryModel categorymodel) {
		this.categorymodel = categorymodel;
	}
	
	
	
}