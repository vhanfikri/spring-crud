package xsis.latihan.repositories;

import org.springframework.stereotype.Repository;

import xsis.latihan.models.ProductModel;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository

public interface ProductRepo extends JpaRepository<ProductModel, Long>{

}
